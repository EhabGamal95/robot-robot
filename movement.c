
#include "parts.h"

void movement (void){
int userpick1;
int userpick2;
int volt_entry;
int retry = 1;
do {
printf("\n\nCHOOSE THE PART YOU WANT TO MOVE \n1-RIGHT ARM\t 2-LEFT ARM \n3-RIGHT LEG\t 4-LEFT LEG\n");
scanf("%i", &userpick1);
switch (userpick1){
 case 1:
     printf("\nYOU CHOSE THE RIGHT ARM:\nCHOOSE SUBPART FROM BELOW \n1-Shoulder\t 2-Elbow\t 3-Wrist\t 4-Fingers\n");
     scanf("%i", &userpick2);
     switch (userpick2){
    case 1:
        printf("YOU CHOSE SHOULDER\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        RIGHT_arm.ARM_shoulder.volt = volt_entry;
        break;
    case 2:
        printf("YOU CHOSE ELBOW\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        RIGHT_arm.ARM_elbow.volt = volt_entry;
        break;
    case 3:
        printf("YOU CHOSE WRIST\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        RIGHT_arm.ARM_wrist.volt = volt_entry;
        break;
    case 4:
        printf("YOU CHOSE FINGERS\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        RIGHT_arm.ARM_fingers.volt = volt_entry;
        break;
     }
        break;
 case 2:
     printf("\nYOU CHOSE THE LEFT ARM:\nCHOOSE SUBPART FROM BELOW \n1-Shoulder\t 2-Elbow\t 3-Wrist\t 4-Fingers\n");
     scanf("%i", &userpick2);
     switch (userpick2){
    case 1:
        printf("YOU CHOSE SHOULDER\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        LEFT_arm.ARM_shoulder.volt = volt_entry;
        break;
    case 2:
        printf("YOU CHOSE ELBOW\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        LEFT_arm.ARM_elbow.volt = volt_entry;
        break;
    case 3:
        printf("YOU CHOSE WRIST\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        LEFT_arm.ARM_wrist.volt = volt_entry;
        break;
    case 4:
        printf("YOU CHOSE FINGERS\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        LEFT_arm.ARM_fingers.volt = volt_entry;
        break;
     }
        break;
 case 3:
     printf("\nYOU CHOSE THE RIGHT LEG:\nCHOOSE SUBPART FROM BELOW \n1-Waist\t 2-Knee\t 3-Foot\n");
     scanf("%i", &userpick2);
     switch (userpick2){
    case 1:
        printf("YOU CHOSE WAIST\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        RIGHT_leg.LEG_waist.volt = volt_entry;
        break;
    case 2:
        printf("YOU CHOSE KNEE\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        RIGHT_leg.LEG_knee.volt = volt_entry;
        break;
    case 3:
        printf("YOU CHOSE FOOT\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        RIGHT_leg.LEG_foot.volt = volt_entry;
        break;
     }
        break;
 case 4:
     printf("\nYOU CHOSE THE LEFT LEG:\nCHOOSE SUBPART FROM BELOW \n1-Waist\t 2-Knee\t 3-Foot\n");
     scanf("%i", &userpick2);
     switch (userpick2){
    case 1:
        printf("YOU CHOSE WAIST\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        LEFT_leg.LEG_waist.volt = volt_entry;
        break;
    case 2:
        printf("YOU CHOSE KNEE\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        LEFT_leg.LEG_knee.volt = volt_entry;
        break;
    case 3:
        printf("YOU CHOSE FOOT\n");
        printf("PLEASE ENTER THE NEEDED VOLT TO MOVE\n");
        scanf("%i", &volt_entry);
        LEFT_leg.LEG_foot.volt = volt_entry;
        break;
     }
    break;

    }
printf("IF YOU WANT TO ENTER ANOTHER VOLTAGE PRESS 1 IF NOT PRESS ANY OTHER KEY");
scanf ("%i", &retry);
} while (retry == 1);
}

