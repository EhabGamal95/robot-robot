
struct foot{
int degree;
int volt;
};

struct knee{
int degree;
int volt;
struct foot *foot_pointer;
};

struct waist_end{
int degree;
int volt;
struct knee *knee_pointer;
};

struct leg{
struct waist_end LEG_waist;
struct knee LEG_knee;
struct foot LEG_foot;
}LEFT_leg, RIGHT_leg;

struct fingers{
int degree;
int volt;
};

struct wrist{
int degree;
int volt;
struct fingers *finger_pointer;
};

struct elbow{
int degree;
int volt;
struct wrist *wirst_pointer;
};


struct shoulder{
 int degree;
 int volt;
 struct elbow *elbow_pointer;
};

struct arm {
struct shoulder ARM_shoulder;
struct elbow ARM_elbow;
struct wrist ARM_wrist;
struct fingers ARM_fingers;
} LEFT_arm, RIGHT_arm;
